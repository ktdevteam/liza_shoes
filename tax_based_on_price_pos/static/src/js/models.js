odoo.define('tax_based_on_price.models', function (require) {
    "use strict";
    
    var models = require('point_of_sale.models');
    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
//    var ajax = require('tax_based_on_price.ajax');
    models.load_fields("account.tax", ["python_compute","python_applicable","js_compute"]);
    
    var _super_posmodel = models.PosModel.prototype;
    models.Orderline = models.Orderline.extend({
        get_applicable_taxes: function(){
            var i;
            // Shenaningans because we need
            // to keep the taxes ordering.
            var ptaxes_ids = this.get_product().taxes_id;
            var ptaxes_set = {};
            for (i = 0; i < ptaxes_ids.length; i++) {
                ptaxes_set[ptaxes_ids[i]] = true;
            }
            var taxes = [];
//            var pos=this.pos;
            for (i = 0; i < this.pos.taxes.length; i++) {
                if (ptaxes_set[this.pos.taxes[i].id]) {
                    var tax=this.pos.taxes[i];
                    if (tax.amount_type==='code' && tax.js_compute){
                        var price_unit=this.get_unit_price();
                        var quantity=this.get_quantity();
                        var base = round_pr(price_unit * quantity);
                        var tax_amount=eval(tax.js_compute);
                        if (tax_amount!==0.0){
                            var fisc_tax = this._map_tax_fiscal_position(tax);
                            console.log("fisc_taxfisc_tax",fisc_tax);
                            taxes.push(fisc_tax);
                        }
                    }
                    else {
                        taxes.push(this.pos.taxes[i]);
                    }
                }
            }
            console.log("taxestaxestaxestaxes",taxes);
            return taxes;
        },
        
        compute_all: function(taxes, price_unit, quantity, currency_rounding) {
            var self = this;
            var list_taxes = [];
            var currency_rounding_bak = currency_rounding;
            if (this.pos.company.tax_calculation_rounding_method == "round_globally"){
               currency_rounding = currency_rounding * 0.00001;
            }
            var total_excluded = round_pr(price_unit * quantity, currency_rounding);
            console.log('total_excluded=====',total_excluded)
            var total_included = total_excluded;
            var base = total_excluded;
            var self=this;
            _(taxes).each(function(tax) {
                if (tax.amount_type === 'group'){
                    tax = self._map_tax_fiscal_position(tax);
                    var ret = self.compute_all(tax.children_tax_ids, price_unit, quantity, currency_rounding);
                    total_excluded = ret.total_excluded;
                    base = ret.total_excluded;
                    total_included = ret.total_included;
                    list_taxes = list_taxes.concat(ret.taxes);
                }
                else if(tax.amount_type === 'code' && tax.js_compute){
                    var tax_amount=0.00;
                    tax_amount=eval(tax.js_compute);
                    console.log('amt---------------',tax_amount)
                    if (tax_amount!==0.0){
                        var fisc_tax = self._map_tax_fiscal_position(tax);
                        if (fisc_tax && fisc_tax.id!==tax.id){
                            var taxes_data=self.compute_all([fisc_tax], price_unit, quantity, currency_rounding);
                            console.log('-------------taxes_data',taxes_data)
                            total_excluded=taxes_data.total_excluded;
                            base = taxes_data.total_excluded;
                            total_included = taxes_data.total_included;
                            list_taxes=list_taxes.concat(taxes_data.taxes);
                        }
                        else{
                            tax_amount = round_pr(tax_amount, currency_rounding);
                            if (tax.price_include) {
                                total_excluded -= tax_amount;
                                base -= tax_amount;
                            }
                            else {
                                total_included += tax_amount;
                            }
                            if (tax.include_base_amount) {
                                base += tax_amount;
                            }
                            var data = {
                                id: tax.id,
                                amount: tax_amount,
                                name: tax.name,
                            };
                            list_taxes.push(data);
                        }
                    }
                } 
                else {
                    tax = self._map_tax_fiscal_position(tax);
                    var tax_amount = self._compute_all(tax, base, quantity);
                    tax_amount = round_pr(tax_amount, currency_rounding);
                    if (tax_amount){
                        if (tax.price_include) {
                            total_excluded -= tax_amount;
                            base -= tax_amount;
                        }
                        else {
                            total_included += tax_amount;
                        }
                        if (tax.include_base_amount) {
                            base += tax_amount;
                        }
                        var data = {
                            id: tax.id,
                            amount: tax_amount,
                            name: tax.name,
                        };
                        list_taxes.push(data);
                    }
                }
            });
            return {
                taxes: list_taxes,
                total_excluded: round_pr(total_excluded, currency_rounding_bak),
                total_included: round_pr(total_included, currency_rounding_bak)
            };
        },
    });
   
})

