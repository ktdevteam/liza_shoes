from odoo import models, fields, api
from odoo.tools.safe_eval import safe_eval


class AccountTaxPython(models.Model):
    _inherit = "account.tax"
    
    js_compute = fields.Text(string='JavaScript Code',
        help="Compute the amount of the tax by setting the variable 'result'.\n\n"
            ":param base_amount: float, actual amount on which the tax is applied\n"
            ":param price_unit: float\n"
            ":param quantity: float\n"
            ":param company: res.company recordset singleton\n"
            ":param product: product.product recordset singleton or None\n"
            ":param partner: res.partner recordset singleton or None")