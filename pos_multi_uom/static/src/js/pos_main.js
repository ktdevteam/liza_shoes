odoo.define('pos_multi_uom.pos_main', function (require) {

"use strict";
    var formats = require('web.formats');
    var utils = require('web.utils');
    var round_di = utils.round_decimals;
    var pos_model = require('point_of_sale.models');
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var _modelproto = pos_model.PosModel.prototype;
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');
    var core = require('web.core');
    var _t = core._t;
    var models = _modelproto.models;
    for (var i = 0; i < models.length; i++) {
        var model = models[i];
        if (model.model === 'product.product') {
            model.fields.push('qty_available');
        }
    }
    models.push({
            model: 'product.uom.price',
            fields: ['product_id', 'uom_id', 'price_retail'],
            loaded: function (self, uom_price) {
                self.uom_price = {};
                for (var i = 0; i < uom_price.length; i++) {
                    self.uom_price[uom_price[i].id] = uom_price[i];
                }
            }
        }
    );
    var PosModelSuper = pos_model.PosModel;
    pos_model.PosModel = pos_model.PosModel.extend({
        initialize: function (session, attributes) {
            PosModelSuper.prototype.initialize.call(this, session, attributes);
            this.set({'uom_price': null, 'sequence': null});
        }
    });

    var SelectUomWidget = PopupWidget.extend({
        template: 'SelectUomWidget',
        init: function (parent, options) {
            this._super(parent, options);
        },
        show: function () {
            var self = this;
            this._super();
            this.renderElement();
        },
        get_product_from_order: function (pos) {
            var order = pos.get_order();
            if (order.get_selected_orderline()) {
                var product = order.get_selected_orderline().get_product();
                if (product) {
                    return product;
                } else {
                    return undefined;
                }
            }
        },
        renderElement: function () {
            var self = this;
            this._super();
            var product = this.get_product_from_order(self.pos);
            if (product != undefined) {
                _.each(this.pos.uom_price, function (uom_price) {

                    if (uom_price.product_id[0] == product.id) {
                        var uom_id = self.pos.units_by_id[uom_price.uom_id[0]];
                        if (uom_id!=self.pos.units_by_id[product.uom_id[0]]){
                            var button = new UomButtonWidget(self, {
                                pos: self.pos,
                                pos_widget: self.pos_widget,
                                uom_price: uom_id,
                                product: product,
                            });
                            button.appendTo($('.placeholder-UomButtonWidget'));
                    };}
                });
                var button = new UomButtonWidget(self, {
                    pos: self.pos,
                    pos_widget: self.pos_widget,
                    uom_price: self.pos.units_by_id[product.uom_id[0]],
                    product: product,
                });
                button.appendTo($('.placeholder-UomButtonWidget'));
            }
        },
        hide: function () {
            this.$el.hide();
        },
        click_confirm: function(){

        this.gui.close_popup();
        if( this.options.confirm ){
            this.options.confirm.call(this,this.inputbuffer);
        }
    },
    });
    gui.define_popup({name:'uom-popup', widget: SelectUomWidget});

    var UomButtonWidget = screens.ScreenWidget.extend({
        template: 'UomButtonWidget',
        init: function (parent, options) {
            this._super(parent, options);
            this.uom_price = options.uom_price;
            this.product = options.product;
        },
        renderElement: function () {
            var self = this;
            this._super();
            if (this.uom_price != undefined) {
                var order = self.pos.get_order();
                var line = order.get_selected_orderline();
                this.$el.click(function () {
                    order.set_unit_product(self.uom_price);
                    order.change_uom_of_product(line, self.product);
                    self.gui.close_popup();
                });
            }
        }
    });

    var MultiUomWidget = screens.ActionButtonWidget.extend({
        template: 'MultiUomWidget',
        button_click: function(){
        var self = this;
        self.gui.show_popup('uom-popup');
        }
        });
    screens.define_action_button({
    'name': 'multi-uom',
    'widget': MultiUomWidget,
    });
    var OrderSuper = pos_model.Order;
    pos_model.Order = pos_model.Order.extend({
        initialize: function (attributes,options) {
            OrderSuper.prototype.initialize.call(this, arguments,options);
            this.unit_product = null;
        },
        set_unit_product: function (unit) {
            this.unit_product = unit;
            this.trigger('change', this);
        },
        get_unit_product: function (unit) {
            return this.unit_product;
        },
        addProduct: function (product, options) {
            var self = this;
            var is_set_price = false;
            if (this._printed) {
                this.destroy();
                return this.pos.get_order().addProduct(product, options);
            }
            options = options || {};
            var attr = JSON.parse(JSON.stringify(product));
            attr.pos = this.pos;
            attr.order = this;
            var line = new module.Orderline({}, {pos: this.pos, order: this, product: product});
            if (options.quantity !== undefined) {
                line.set_quantity(options.quantity);
            }
            if (options.price !== undefined) {
                line.set_unit_price(options.price);
            }
            if (options.discount !== undefined) {
                line.set_discount(options.discount);
            }
            if(options.extras !== undefined){
                for (var prop in options.extras) {
                    line[prop] = options.extras[prop];
                }
            }
            var last_orderline = this.getLastOrderline();
            if (last_orderline && last_orderline.can_be_merged_with(line) && options.merge !== false) {
                last_orderline.merge(line);
            } else {
               this.orderlines.add(line);
            }
            this.select_orderline(this.getLastOrderline());
            if(line.has_product_lot){
                this.display_lot_popup();
            }
            this.unit_product = null;

        },

        change_uom_of_product: function (line, product) {
            var self = this;
            var is_set_price = false;
            var unit_product = this.get_unit_product();
            if(product.uom_id[0] === unit_product.id){
                line.set_unit_product(product.uom_id[0]);
                line.set_unit_price(product.price)
                is_set_price = true;
            }
            else{
                _.each(this.pos.uom_price, function (uom_price) {
                    if (uom_price.product_id[0] == product.id && uom_price.uom_id[0] == unit_product.id) {
                        line.set_unit_product(uom_price.uom_id[0]);
                        line.set_unit_price(uom_price.price_retail);
                        is_set_price = true;
                    }
                });
            }
        }
    });
    
    var OrderlineSuper = pos_model.Orderline;
    pos_model.Orderline = pos_model.Orderline.extend({
        initialize: function (attr, options) {
            OrderlineSuper.prototype.initialize.call(this, attr, options);
            this.uom_product= null;
        },
        get_quantity_str: function () {
            return  this.quantityStr.slice(0, -1);
        },
        set_unit_product: function (unit) {
            this.uom_product = unit;
            this.trigger('change', this);
        },
        get_unit_product: function () {
            return this.uom_product;
        },
        get_unit: function () {
            var unit_id = this.product.uom_id;
            var unit_product = this.get_unit_product();
            if (!unit_id) {
                return undefined;
            }
            unit_id = unit_id[0];
            if (!this.pos) {
                return undefined;
            }
            if (unit_product != null) {
                return this.pos.units_by_id[unit_product];
            }
            else {
                return this.pos.units_by_id[unit_id];
            }
        },
        export_as_JSON: function() {
            var pack_lot_ids = [];
            if (this.has_product_lot){
                this.pack_lot_lines.each(_.bind( function(item) {
                    return pack_lot_ids.push([0, 0, item.export_as_JSON()]);
                }, this));
            }
            return {
                qty: this.get_quantity(),
                price_unit: this.get_unit_price(),
                discount: this.get_discount(),
                product_id: this.get_product().id,
                tax_ids: [[6, false, _.map(this.get_applicable_taxes(), function(tax){ return tax.id; })]],
                id: this.id,
                pack_lot_ids: pack_lot_ids,
                unit_id:this.get_unit().id,
            };
        }
    });

    PosBaseWidget.include({
        tiki_format_currency: function (amount) {
            var currency = (this.pos && this.pos.currency) ? this.pos.currency : {symbol: '$', position: 'after', rounding: 0.01, decimals: 2};
            var decimals = this.pos.dp['Product Price'];
            if (typeof amount === 'number') {
                amount = round_di(amount, decimals).toFixed(decimals);
                amount = formats.format_value(round_di(amount, decimals), { type: 'float', digits: [69, decimals]});
            }
            return amount
        }
    });

    screens.OrderWidget.include({
        refresh: function () {
            this.renderElement();
        },
        update_summary: function(){
            var order = this.pos.get_order();
            if (!order.get_orderlines().length) {
                return;
            }

            var total     = order ? order.get_total_with_tax() : 0;
            var taxes     = order ? total - order.get_total_without_tax() : 0;

            this.el.querySelector('.summary .total > .value').textContent = this.tiki_format_currency(total);
            this.el.querySelector('.summary .total .subentry .value').textContent = this.tiki_format_currency(taxes);
        },
        set_value: function (val) {
            var order = this.pos.get_order();
            if (order.get_selected_orderline()) {
                var mode = this.numpad_state.get('mode');
                if (mode === 'quantity') {
                    order.get_selected_orderline().set_quantity(val);
                }else if( mode === 'discount'){
                order.get_selected_orderline().set_discount(val);
                } else if (mode === 'price') {
                    order.get_selected_orderline().set_unit_price(val);
                }
            }
        },
    });

return{
    MultiUomWidget:MultiUomWidget,
    SelectUomWidget:SelectUomWidget,
    UomButtonWidget:UomButtonWidget,
}
});