# -*- coding: utf-8 -*-
{
    "name": "POS Multi UOM",
    "version": "10.0",
    "website": "knacktechs.com",
    "category": "POS",
    "depends": [
        'web','point_of_sale', 'product',
    ],
    "description": "Point Of Sale",
    "data": [
        'security/ir.model.access.csv',
        'pos_multi_uom.xml',
        'views/product_product_view.xml'
    ],
    "qweb": ['static/src/xml/pos_tmp.xml',
    ],
    "auto_install": False,
    "installable": True,
    'certificate': '',
}
