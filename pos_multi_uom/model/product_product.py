from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from odoo import fields, models


class ProductProduct(models.Model):
    _inherit = 'product.product'
#    wholesale_price =  fields.Float('Wholesale Price', digits=dp.get_precision('Product Price'))
    use_uom_prices = fields.Boolean(
        'Use UOM Prices?',
        help='Use different prices for different UOMs?')
    uom_category_id = fields.Many2one(
        'product.uom.categ',
        string='UOM Category', related='uom_id.category_id')
    uom_price_ids = fields.One2many(
        'product.uom.price', 'product_id', string='UOM Prices')


class ProductUomPrice(models.Model):

    """"""
    _name = 'product.uom.price'
    _description = 'Product Uom Price'

    product_id = fields.Many2one(
        'product.product',
        string='Product',
        required=True,)
    uom_id = fields.Many2one('product.uom', string='UOM', required=True,)
    price_retail = fields.Float(
        'Retail Price', digits=dp.get_precision('Product Price'), required=True)
#    price_wholesale = fields.Float(
#        'Wholesale Price', digits=dp.get_precision('Product Price'), required=True)

    _sql_constraints = [
        ('price_uniq', 'unique(product_id, uom_id)',
            'UOM mast be unique per Product Template!'),
    ]