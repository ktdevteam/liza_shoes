# -*- coding: utf-8 -*-

from openerp import fields, models,api,_

class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.multi
    def create_picking(self):
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            if all(t == 'service' for t in order.lines.mapped('product_id.type')):
                continue
            address = order.partner_id and order.partner_id.sudo().address_get(adr_pref=['delivery']) or {}
            picking_type = order.picking_type_id
            picking_id = False
            
            location_id = order.location_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            elif picking_type:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id
            if picking_type:
                pos_qty = all([x.qty >= 0 for x in order.lines])
                picking_id = Picking.create({
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id if pos_qty else destination_id,
                    'location_dest_id': destination_id if pos_qty else location_id,
                })
                message = _("This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (order.id, order.name)
                picking_id.message_post(body=message)
                order.write({'picking_id': picking_id.id})
#            move_list = []
            for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                if line.product_id.uom_id.id != line.unit_id:
                    unit_obj = self.env['product.uom'].browse(line.unit_id)
                    if unit_obj.uom_type == 'bigger':
                        qty = line.qty
                    else:
                        qty = line.qty
                    unit_id = unit_obj.id
                else:
                    unit_id = line.product_id.uom_id.id
                    qty = line.qty
                Move +=Move.create({
                    'name': line.name,
                    'product_uom': unit_id,
                    'product_uos': unit_id,
                    'picking_id': picking_id.id,
                    'picking_type_id': picking_type.id,
                    'product_id': line.product_id.id,
                    'product_uos_qty': abs(qty),
                    'product_uom_qty': abs(qty),
                    'state': 'draft',
                    'location_id': location_id if qty >= 0 else destination_id,
                    'location_dest_id': destination_id if qty >= 0 else location_id,
                })

            if picking_id:
                picking_id.action_confirm()
                picking_id.force_assign()
                order.set_pack_operation_lot()
                picking_id.action_done()
            elif Move:
                Move.action_confirm()
                Move.force_assign()
                Move.action_done()
        return True

class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    unit_id =fields.Integer('Unit ID')