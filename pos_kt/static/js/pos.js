odoo.define('pos_kt', function (require) {
"use strict";

var module = require('point_of_sale.models');
var screens = require('point_of_sale.screens');
var core = require('web.core');
var Model = require('web.DataModel');
var gui = require('point_of_sale.gui');
var QWeb = core.qweb;
var _t = core._t;
var models = module.PosModel.prototype.models;
for (var i = 0; i < models.length; i++) {
        var model = models[i];
        if (model.model === 'res.partner') {
            model.fields.push('mobile');
        }
        if (model.model === 'res.company') {
            model.fields.push('rml_header1','phone','street','street2','city','zip','tag_footer1','tag_footer4','tag_footer5','tag_footer6','tag_footer2','tag_footer3','tag_header1','tag_header2','tag_header3');
        }
        if (model.model === 'product.product') {
            model.fields.push('default_code');
        }
        if (model.model === 'account.tax') {
            model.fields.push('amount_type','python_compute','python_applicable','description','gst_type');
        }
    }
module.load_fields("pos.order", "get_subtotal_in_words");
screens.ClientListScreenWidget = screens.ClientListScreenWidget.include({
        render_list: function(partners){
        var contents = this.$el[0].querySelector('.client-list-contents');
        contents.innerHTML = "";
        for(var i = 0, len = Math.min(partners.length,1000); i < len; i++){
            var partner    = partners[i];
            var clientline = this.partner_cache.get_node(partner.id);
            var clientline_html = QWeb.render('ClientLine',{widget: this, partner:partners[i]});
            var clientline = document.createElement('tbody');
            clientline.innerHTML = clientline_html;
            clientline = clientline.childNodes[1];
            this.partner_cache.cache_node(partner.id,clientline);

            if( partner === this.old_client ){
                clientline.classList.add('highlight');
            }else{
                clientline.classList.remove('highlight');
            }
            contents.appendChild(clientline);
        }
    },
//	save_client_details: function(partner) {
//	    var self = this;
//	    var fields = {};
//	    this.$('.client-details-contents .detail').each(function(idx,el){
//	        fields[el.name] = el.value;
//	    });
//	
//	    if (!fields.name) {
//	        this.gui.show_popup('error',_t('A Customer Name Is Required'));
//	        return;
//	    }
//	    if (!fields.mobile) {
//	        this.gui.show_popup('error',_t('Mobile No Is Required'));
//	        return;
//	    }
//            var value = document.getElementById('mobile1').value;
//            var len = value.toString()
//	    if ( len.length > 10 || len.length < 10) {
//	        this.gui.show_popup('error',_t('Length of mobile no is INVALID'));
//	        return;
//	    }
//	    
//	    if (this.uploaded_picture) {
//	        fields.image = this.uploaded_picture;
//	    }
//	
//	    fields.id           = partner.id || false;
//	    fields.country_id   = fields.country_id || false;
//	    fields.barcode      = fields.barcode || '';
//	
//	    new Model('res.partner').call('create_from_ui',[fields]).then(function(partner_id){
//	        self.saved_client_details(partner_id);
//	    },function(err,event){
//	        event.preventDefault();
//	        self.gui.show_popup('error',{
//	            'title': _t('Error: Could not Save Changes'),
//	            'body': _t('Your Internet connection is probably down.'),
//	        });
//	    });
//	},
});

//screens.ProductListWidget.include({
//    render_product: function(product){
//        var cached = this.product_cache.get_node(product.id);
//        if(!cached){
//            var product_html = QWeb.render('Product',{
//                    widget:  this,
//                    product: product,
//                    image_url: this.get_product_image_url(product),
//                });
//            var product_node = document.createElement('tbody');
//            product_node.innerHTML = product_html;
//            product_node = product_node.childNodes[1];
//            this.product_cache.cache_node(product.id,product_node);
//            return product_node;
//        }
//        return cached;
//    },
//    renderElement: function() {
//        var el_str  = QWeb.render(this.template, {widget: this});
//        var el_node = document.createElement('div');
//            el_node.innerHTML = el_str;
//            el_node = el_node.childNodes[1];
//
//        if(this.el && this.el.parentNode){
//            this.el.parentNode.replaceChild(el_node,this.el);
//        }
//        this.el = el_node;
//
//        var list_container = el_node.querySelector('.product-list-contents');
//        for(var i = 0, len = this.product_list.length; i < len; i++){
//            var product_node = this.render_product(this.product_list[i]);
//            product_node.addEventListener('click',this.click_product_handler);
//            list_container.appendChild(product_node);
//        }
//    },
//});
screens.OrderWidget.include({
    render_orderline: function(orderline){
        var el_str  = QWeb.render('Orderline',{widget:this, line:orderline}); 
        var el_node = document.createElement('tbody');
            el_node.innerHTML = _.str.trim(el_str);
            el_node = el_node.childNodes[0];
            el_node.orderline = orderline;
            el_node.addEventListener('click',this.line_click_handler);
        var el_lot_icon = el_node.querySelector('.line-lot-icon');
        if(el_lot_icon){
            el_lot_icon.addEventListener('click', (function() {
                this.show_product_lot(orderline);
            }.bind(this)));
        }

        orderline.node = el_node;
        return el_node;
    },
});

});
