from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from odoo import fields, models

class ProductMultiPrice(models.Model):
    _name = 'product.multi.price'
    _description = 'Product Uom Price'

    sale_price = fields.Float(
        'Sale Price', digits=dp.get_precision('Product Price'), required=True)