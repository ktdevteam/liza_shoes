from odoo import api, fields, models
import datetime

class PosOrder(models.Model):

    _inherit = "pos.order"
    
    get_subtotal_in_words=fields.Char()
    
    @api.model
    def _order_fields(self, ui_order): #v10
        vals = super(PosOrder, self)._order_fields(ui_order)
        print'_order_fields_order_fields',ui_order
        if 'get_subtotal_in_words' in 'ui_order':
            vals['get_subtotal_in_words']=ui_order['get_subtotal_in_words']
        return vals
    
