from odoo import models, fields, api
from odoo.tools.safe_eval import safe_eval

class AccountTaxPython(models.Model):
    _inherit = "account.tax"
    
    gst_type=fields.Selection([('cgst2.5','CGST2.5%'),('sgst2.5','SGST2.5%'),('cgst6','CGST6%'),('sgst6','SGST6%'),('cgst9','CGST9%'),('sgst9','SGST9%'),('cgst14','CGST14%'),('sgst14','SGST14%')], string="GST")