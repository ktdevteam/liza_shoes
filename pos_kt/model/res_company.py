from openerp import models, fields, api

class res_company(models.Model):
    
    _inherit = "res.company"
    
    tag_footer1 = fields.Text('POS Footer 1', default="This document is to be treated as tax invoice to the extent of supply of the taxable goods, and bill of supply to the extent of supply of exempted goods.")
    tag_footer2 = fields.Text('POS Footer2', default="Customer Copy")
    tag_footer3 = fields.Text('POS Footer3', )
    tag_footer4 = fields.Text('POS Footer4', default="Thank You For visiting LIZA")
    tag_footer5 = fields.Text('POS Footer5', )
    tag_footer6 = fields.Text('POS Footer6', )
    tag_header1 = fields.Text('POS Header1', )
    tag_header2 = fields.Text('POS Header2', )
    tag_header3 = fields.Text('POS Header3',)