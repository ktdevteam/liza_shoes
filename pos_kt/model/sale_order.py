from odoo import api, fields, models,_
from num2words import num2words


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.multi
    @api.onchange('product_id','price_unit')
    def product_id_change(self):
        vals={}
        price_unit=self.price_unit
        res= super(SaleOrderLine, self).product_id_change()
        self.price_unit=price_unit if price_unit!=0.0 else self.price_unit
        active_tax_ids=self.tax_id.search([('active','=',True),('type_tax_use','=','sale')])
        tax_domain = [('active_tax', '=','True'),('id','in',active_tax_ids.ids)]
        partner_state=self.order_id.partner_id.state_id
        company_state=self.order_id.company_id.state_id
        if partner_state == company_state:
            tax_domain +=[('amount_type', '=','group')]
            if self.product_id:
                tax_ids=[]
                for tax_id in self.product_id.taxes_id:
                    if tax_id.amount_type=='code':
                        result=tax_id.compute_all(self.price_unit, self.currency_id, self.product_uom_qty, self.product_id, self.order_id.partner_id)['taxes']
                        if result and result[0].has_key('amount') and result[0].get('amount')>0.00:
                            tax_ids.append(tax_id.id)
                    elif tax_id.amount_type=='group':
                        tax_ids.append(tax_id.id)
                vals = {
                'tax_id': [(6, 0, tax_ids)],
                }
        elif partner_state != company_state:
            tax_domain +=[('amount_type', '!=','group')]
            if self.product_id:
                tax_ids=[]
                for tax_id in self.product_id.taxes_id:
                    if tax_id.amount_type=='code':
                        result=tax_id.compute_all(self.price_unit, self.currency_id, self.product_uom_qty, self.product_id, self.order_id.partner_id)['taxes']
                        if result and result[0].has_key('amount') and result[0].get('amount')>0.00:
                            tax_ids.append(tax_id.id)
                    elif tax_id.amount_type!='group':
                        tax_ids.append(tax_id.id)
                vals = {
                'tax_id': [(6, 0, tax_ids)],
                }
        domain = {'tax_id':tax_domain}
        self.update(vals)
        return {'domain': domain}