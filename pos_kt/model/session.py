from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.addons.point_of_sale.wizard.pos_box import PosBox

class SessionPos(models.Model):
    _inherit='pos.session'
    
    @api.multi
    def calc_statements_total(self):
        total=0.0
        for session in self:
            for statement in session.statement_ids:
                print'statementstatement-----',statement.total_entry_encoding
                total+=statement.total_entry_encoding
        return total
    
    def summary_by_product(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        products = {}  # product_id -> data
#        print'self.browse(cr, uid, ids, context=context)'
        for session in self:
            print'session------------------',session,session.order_ids
            for order in session.order_ids:
                print'session.order_idssession.order_ids',order
                for line in order.lines:
                    id = line.product_id.id
                    if id not in products:
                        products[id] = {'product': line.product_id.name,
                                        'qty': 0,
                                        'total': 0}
                    products[id]['qty'] += line.qty
                    products[id]['total'] += line.price_subtotal_incl
        return products.values()
    
    @api.multi
    def cash_put_in(self):
        money_put_in=0.0
        for session in self:
            for statement in session.statement_ids:
                for statement_line in statement.line_ids:
                    if statement_line.is_cash_in==True:
                        money_put_in+=statement_line.amount
        return money_put_in
    
    def _calc_sales(self):
        res = {}
        for session in self:
            print'session----------------',session
            res[session.id] = {'untaxed_sales': 0}
            for order in session.order_ids:
                for line in order.lines:
                    if not line.product_id.taxes_id:
                        res[session.id]['untaxed_sales'] += line.price_subtotal
        return res
    
    def test(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        account_tax_obj = self.env['account.tax']
        res = {}  # tax_id -> data
        for session in self:
            for order in session.order_ids:
                for line in order.lines:
                    taxes_ids = [tax for tax in line.product_id.taxes_id if tax.company_id.id == line.order_id.company_id.id]
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    print'taxes_ids----------',taxes_ids,line.order_id,price
                    taxes = account_tax_obj.compute_all( price, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)['taxes']
#                    taxes = account_tax_obj.compute_all(taxes_ids, price, line.qty or False)['taxes']
#                    cur = line.order_id.pricelist_id.currency_id
                    print'taxes----------',taxes
#                    # print 'taxes', taxes
#                    for tax in taxes['taxes']:
#                        id = tax['id']
#                        if id not in res:
#                            t = account_tax_obj.browse()
#                            tax_rule = ''
#                            if t.type == 'percent':
#                                tax_rule = str(100 * t.amount) + '%'
#                            else:
#                                tax_rule = str(t.amount)
#                            res[id] = {'name': tax['name'],
#                                       'base': 0,
#                                       'tax': tax_rule,
#                                       'total': 0,
#                                       }
#                        res[id]['base'] += cur.round(tax['price_unit'] * line.qty)
#                        res[id]['total'] += tax['amount']
#                        # cur_obj.round(cr, uid, cur, taxes['amount'])
#
#        return res.values()
    
    @api.multi
    def cash_put_out(self):
        money_put_out=0.0
        for session in self:
            for statement in session.statement_ids:
                for statement_line in statement.line_ids:
                    if statement_line.is_cash_out==True:
                        money_put_out+=statement_line.amount
                        print'money_put_out--------',money_put_out,statement_line.amount
        return money_put_out
    
SessionPos()

class PosBoxIn(PosBox):
    _inherit = 'cash.box.in'

    def _calculate_values_for_statement_line(self,record):
        values = super(PosBoxIn, self)._calculate_values_for_statement_line(record)
        values['is_cash_in'] = True
        return values


class PosBoxOut(PosBox):
    _inherit = 'cash.box.out'

    def _calculate_values_for_statement_line(self,record):
        values = super(PosBoxOut, self)._calculate_values_for_statement_line(record)
        values['is_cash_out'] = True
        return values
    
class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"


    is_cash_in= fields.Boolean('Cash In', default=False)
    is_cash_out= fields.Boolean('Cash Out', default=False)
    
    @api.multi
    def _get_sum_entry_encoding_put_in(self):
        print'test--------',self.statement_ids
        res = {}
        for statement in self:
            print'statement.line_ids----',statement
            res[statement.id] = sum((line.amount for line in statement.line_ids if line.is_cash_in), 0.0)
            print'res0---------',res
        return res
    
    @api.multi
    def _get_sum_entry_encoding_take_out(self):
        res = {}
        for statement in self:
            print'statement.line_ids----',statement
            res[statement.id] = sum((line.amount for line in statement.line_ids if line.is_cash_out), 0.0)
            print'res0---------',res
        return res