# -*- encoding: utf-8 -*-
##############################################################################
#
#    Authors: sunil, 
#    Copyright Knacktechs SA 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'POS KT',
    'description': """

""",
    'version': '1.0',
    'author': "Knacktechs Solutions",
    'license': '',
    'website': 'http://www.knacktechs.com',
    'images': [],
    'depends': ['point_of_sale'],
    'demo': [],
    'data': [  
                'views/res_company_view.xml',
                'views/account_tax.xml',
                'views/point_of_sale.xml',
                'views/report.xml',
                'views/report_sessionsummary.xml',
                'views/report_sessions.xml',
                
             ],
    # tests order matter
    'test': [
             ],
    # 'tests/account_move_line.yml'
    'qweb': ['static/xml/pos.xml'],
    'active': False,
    'installable': True,
    'application': True,
}
