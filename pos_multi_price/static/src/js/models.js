odoo.define('pos_multi_price.models', function (require) {
"use strict";

var models = require('point_of_sale.models');

models.load_models({
        model: 'product.multi.price',
        fields: ['price','currency_id','company_id'],
        loaded: function (self, prod_price) {
            self.prod_price = {};
            for (var i = 0; i < prod_price.length; i++) {
                self.prod_price[prod_price[i].id] = prod_price[i];
            }
        }
    }
);

var PosModelSuper = models.PosModel;
models.PosModel = models.PosModel.extend({
    initialize: function (session, attributes) {
        PosModelSuper.prototype.initialize.call(this, session, attributes);
        this.set({'prod_price': 0.00, 'sequence': null});
    }
});

var OrderSuper = models.Order;
models.Order = models.Order.extend({

    initialize: function (attributes,options) {
        OrderSuper.prototype.initialize.call(this, arguments,options);
        this.product_price = 0.00;
    },
    set_product_price: function (price) {
        this.product_price = price;
        this.trigger('change', this);
    },
    get_product_price: function (price) {
        return this.product_price;
    },
    addProduct: function (product, options) {
        var self = this;
        var is_set_price = false;
        if (this._printed) {
            this.destroy();
            return this.pos.get_order().addProduct(product, options);
        }
        options = options || {};
        var attr = JSON.parse(JSON.stringify(product));
        attr.pos = this.pos;
        attr.order = this;
        var line = new models.Orderline({}, {pos: this.pos, order: this, product: product});
        if (options.quantity !== undefined) {
            line.set_quantity(options.quantity);
        }
        if (options.price !== undefined) {
            line.set_unit_price(options.price);
        }
        if (options.discount !== undefined) {
            line.set_discount(options.discount);
        }
        if(options.extras !== undefined){
            for (var prop in options.extras) {
                line[prop] = options.extras[prop];
            }
        }
        var last_orderline = this.getLastOrderline();
        if (last_orderline && last_orderline.can_be_merged_with(line) && options.merge !== false) {
            last_orderline.merge(line);
        } else {
           this.orderlines.add(line);
        }
        this.select_orderline(this.getLastOrderline());
        if(line.has_product_lot){
            this.display_lot_popup();
        }
        this.product_price = 0.00;

    },

    change_price_of_product: function (line, product) {
        var product_price = this.get_product_price();
        if(line.price === product_price){
            line.set_unit_price(line.price);
        }
        else{
            this.set_product_price(product_price.price);
            line.set_unit_price(product_price.price);
            }
        }
    }
);

return models;
});