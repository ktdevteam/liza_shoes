odoo.define('pos_multi_price.screens', function (require) {
"use strict";

var screens = require('point_of_sale.screens');
var core = require('web.core');
var _t = core._t;

var MultiPriceWidget = screens.ActionButtonWidget.extend({
    template: 'MultiPriceWidget',
    init: function (parent, options) {
        this._super(parent, options);
        this.prod_price = options.prod_price;
        this.product = options.product;
    },
    button_click: function(){
    var self = this;
    var prod_prices_list = _.map(self.pos.prod_price, function (prod_price) {
        return {
            label: prod_price.price,
            item: prod_price
        };
    });
    console.log("prod_pricesprod_prices",prod_prices_list);
    self.gui.show_popup('selection',{
        title: _t('Select Price'),
        list: prod_prices_list,
        confirm: function (prod_price) {
            var order = self.pos.get_order();
            var line = order.get_selected_orderline();
            console.log("self.prod_priceself.prod_priceself.prod_price",prod_price);
            order.set_product_price(prod_price);
            order.change_price_of_product(line, self.product);
            self.gui.close_popup();
        }
    });
    }
    });
screens.define_action_button({
'name': 'multi-price',
'widget': MultiPriceWidget,
});

screens.OrderWidget.include({
    refresh: function () {
        this.renderElement();
    },
    set_value: function (val) {
        var order = this.pos.get_order();
        if (order.get_selected_orderline()) {
            var mode = this.numpad_state.get('mode');
            if (mode === 'quantity') {
                order.get_selected_orderline().set_quantity(val);
            }else if( mode === 'discount'){
            order.get_selected_orderline().set_discount(val);
            } else if (mode === 'price') {
                order.get_selected_orderline().set_unit_price(val);
            }
        }
    },
});

return MultiPriceWidget;
})