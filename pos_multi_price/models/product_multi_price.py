from odoo import fields,models

class ProductMutliPrice(models.Model):
    _name='product.multi.price'
    _rec_name='price'
    
    
    company_id=fields.Many2one(comodel_name='res.company',default=lambda self: self.env['res.company']._company_default_get('product.multi.price'))
    currency_id=fields.Many2one(comodel_name='res.currency',related='company_id.currency_id')
    price=fields.Monetary(string='Price',currency_field='currency_id')