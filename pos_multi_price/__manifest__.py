# -*- coding: utf-8 -*-
{
    "name": "POS Multi Price",
    'author': "Knacktechs Solutions",
    "version": "10.0",
    "website": "knacktechs.com",
    "category": "POS",
    "depends": [
        'web','point_of_sale', 'product',
    ],
    "description": "Point Of Sale",
    "data": [
        'security/ir.model.access.csv',
        'views/product_multi_price_view.xml',
        'views/pos.xml'
    ],
    "qweb": [
    'static/src/xml/pos_multi_price.xml',
    ],
    "auto_install": False,
    "installable": True,
    'certificate': '',
}
