from odoo import api, fields, models, _

class AccountTax(models.Model):
    _inherit = 'account.tax'
    
    active_tax=fields.Boolean('Tax Display')