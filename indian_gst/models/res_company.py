from openerp import models, fields, api

class res_company(models.Model):
    
    _inherit = "res.company"
    
    vat= fields.Char(string='GSTIN', size=15)
    sale_line=fields.Integer('Sale Lines')
    inv_line=fields.Integer('Invoice Lines')
    purchase_line=fields.Integer('Purchase Lines')
    pan_no=fields.Char('Pan No')
    
    @api.onchange('vat')
    def onchange_gst(self):
        state_obj=self.env['res.country.state']
        if self.vat:
            value=self.vat[:2]
            state_id=state_obj.search([('code','=',value),('country_id','=',self.country_id.id)])
            if state_id.code>=36:
                self.state_id=state_id.id
            else:
                raise RedirectWarning(_('Please enter valid GST number'))