from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning

class Partner(models.Model):
    _inherit = 'res.partner'
    
    gst = fields.Char(string='GST No', size=15)
    pan_no=fields.Char('Pan No')
    
    @api.onchange('gst')
    def onchange_gst(self):
        state_obj=self.env['res.country.state']
        if self.gst:
            value=self.gst[:2]
            state_id=state_obj.search([('code','=',value),('country_id','=',self.company_id.country_id.id)])
            if state_id.code>=36:
                self.state_id=state_id.id
            else:
                raise RedirectWarning(_('Please enter valid GST number'))
Partner()