from odoo import models, fields, api, _
from odoo.osv import expression, osv
from datetime import datetime,date
import re
from base64 import b64decode
import logging
import datetime
LOG = logging.getLogger("dicttoxml")
import binascii
import os
import string
import time
import xlwt
import cStringIO
import base64
#from .. import format_common
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition

class Binary(http.Controller):
 @http.route('/opt/download', type='http', auth="public")
 @serialize_exception
 def download_document(self,model,field,id,filename=None, **kw):
    """ Download link for files stored as binary fields.
    :param str model: name of the model to fetch the binary from
    :param str field: binary field
    :param str id: id of the record from which to fetch the binary
    :param str filename: field holding the file's name, if any
    :returns: :class:`werkzeug.wrappers.Response`
    """
    cr, uid, context = request.cr, request.uid, request.context
    env = api.Environment(cr, 1, {})  
    out_brw=env['output'].browse(int(id))
    filecontent = base64.b64decode(out_brw.xls_output.encode('utf-8') or '')
    if not filecontent:
        return request.not_found()
    else:
       if not filename:
           filename = '%s_%s' % (model.replace('.', '_'), id)
       return request.make_response(filecontent,
                      [('Content-Type', 'application/octet-stream'),
                       ('Content-Disposition', content_disposition(filename))])  
                       
class GstReturnOne(models.TransientModel):
    _name = "gst.return.one.report"
    _description = 'GST Return One Report'
    
    start_date=fields.Date('From')
    end_date=fields.Date('To')
    @api.multi
    def gst_return_one(self):
        cr= self.env.cr
        start_date=self.start_date
        print'start_date---',type(start_date)
        end_date=self.end_date
        inv_obj = self.env['account.invoice']
        company_obj = self.env['res.company']
#        self.env.cr.execute("select id from account_invoice where date_invoice::date between %s and %s",(start_date,end_date))
#        inv_ids = map(lambda x:x[0], self.env.cr.fetchall())
#        print'inv_ids===========',inv_ids
        inv_ids=inv_obj.search([('date_invoice','>=',start_date),('date_invoice','<=',end_date),('state','in',('open','paid')),('type','in',('out_invoice','out_refund'))])
        print'---------------------',inv_ids
        self.env.cr.execute("select id from res_company")
        company_id = map(lambda x:x[0], self.env.cr.fetchall())
        res_company=company_obj.search([('id','in',company_id)])
        print'res_company=========',company_id
        if inv_ids:
            workbook = xlwt.Workbook()
            sheet = workbook.add_sheet('GSTR-1')
            style1 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 300; borders: left thin, top thin, bottom thin,right thin')
            style2 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 200; borders: left thin, top thin, bottom thin,right thin')
            style3 = xlwt.easyxf('font: bold on; borders: left thin, top thin, bottom thin,right thin')
            Hedaer_Text = 'GSTR-1'
            sheet.write_merge(0, 1, 0, 7, 'Form GSTR-1', style1)
            sheet.write_merge(2, 3, 0, 7, '[See Rule.....]', style2)
            sheet.write_merge(5, 6, 0, 7, 'DETAILS OF OUTWARD SUPPLIES', style1)
            sheet.write_merge(8, 8, 0, 5, )
            date=datetime.datetime.strptime(start_date, "%Y-%m-%d").strftime('%Y')
            sheet.write(8, 6, 'Year', style3 )
            sheet.write(8, 7, date)
            sheet.write_merge(9, 9, 0, 5, )
            date=datetime.datetime.strptime(start_date, "%Y-%m-%d").strftime('%B')
            sheet.write(9, 6, 'Month', style3 )
            sheet.write(9, 7, date )
            sheet.write_merge(10, 10, 1, 5, 'GSTIN', style2)
            sheet.write_merge(10, 10, 6, 7, res_company.vat, style2)
            sheet.write_merge(11, 11, 1, 5, 'Name of the Taxable Person', style2)
            sheet.write_merge(11, 11, 6, 9, res_company.name, style2)
            sheet.write_merge(12, 12, 1, 5, 'Aggregate Turnover of the Taxable Person in the previous FY', style2)
            sheet.write_merge(13, 13, 1, 5, 'Period', style2 )
            sheet.write_merge(15, 16, 0, 1, 'GSTIN/UIN', style2 )
            sheet.write_merge(15, 15, 2, 17, 'Invoice', style2 )
            sheet.write_merge(15, 15, 18, 19, 'IGST', style2 )
            sheet.write_merge(15, 15, 20, 21, 'CGST', style2 )
            sheet.write_merge(15, 15, 22, 23, 'SGST', style2 )
            sheet.write_merge(15, 15, 24, 25, 'Cess', style2 )
            sheet.write_merge(15, 16, 26, 27, 'Place Of Supply', style2 )
            sheet.write_merge(16, 16, 2, 3, 'Type', style2 )
            sheet.write_merge(16, 16, 4, 5, 'Invoice No', style2 )
            sheet.write(16, 6, 'Date', style2 )
            sheet.write_merge(16, 16, 7, 8, 'HSN', style2 )
            sheet.write_merge(16, 16, 9, 12, 'Goods/Services', style2 )
            sheet.write(16, 13, 'Qty', style2 )
            sheet.write(16, 14, 'Rate', style2 )
            sheet.write(16, 15, 'Value', style2 )
            sheet.write_merge(16, 16, 16, 17, 'Taxable value', style2 )
            sheet.write(16, 18, 'Rate', style2 )
            sheet.write(16, 19, 'Amt', style2 )
            sheet.write(16, 20, 'Rate', style2 )
            sheet.write(16, 21, 'Amt', style2 )
            sheet.write(16, 22, 'Rate', style2 )
            sheet.write(16, 23, 'Amt', style2 )
            sheet.write(16, 24, 'Rate', style2 )
            sheet.write(16, 25, 'Amt', style2 )
            line_cntr=17
            for inv_id in inv_ids:
                if inv_id.type=='out_refund' or inv_id.type=='out_invoice':
                    for inv_line_id in inv_id.invoice_line_ids:
                        sheet.write_merge(line_cntr, line_cntr, 0, 1, inv_id.partner_id.gst, style2 )
                        if inv_id.type=='out_refund':
                            sheet.write_merge(line_cntr, line_cntr, 2, 3, 'Refund Invoice' )
                        if inv_id.type=='out_invoice':
                           sheet.write_merge(line_cntr, line_cntr, 2, 3, "Invoice" )
                        sheet.write_merge(line_cntr, line_cntr, 4, 5, inv_id.number )
                        sheet.write(line_cntr, 6, inv_id.date_invoice )
                        sheet.write_merge(line_cntr, line_cntr, 7, 8, inv_line_id.product_id.default_code )
                        sheet.write_merge(line_cntr, line_cntr, 9, 12, inv_line_id.name )
                        sheet.write(line_cntr, 13, inv_line_id.quantity )
                        sheet.write(line_cntr, 14, inv_line_id.price_unit )
                        sheet.write(line_cntr, 15, inv_line_id.price_subtotal )
                        sheet.write_merge(line_cntr, line_cntr, 16, 17, inv_id.amount_total )
                        if inv_line_id.invoice_line_tax_ids:
                            for tax_id in inv_line_id.invoice_line_tax_ids:
                                if tax_id.amount_type!='group':
                                    sheet.write(line_cntr, 18, tax_id.amount )
                                    sheet.write(line_cntr, 19, (tax_id.amount*inv_line_id.price_subtotal)/100 )
                                if tax_id.amount_type=='group':
                                    for tax_group_line in tax_id.children_tax_ids:
                                        if tax_group_line.description=='CGST':
                                            sheet.write(line_cntr, 20, tax_group_line.amount )
                                            sheet.write(line_cntr, 21, (tax_group_line.amount*inv_line_id.price_subtotal)/100 )
                                        if tax_group_line.description=='SGST':
                                            sheet.write(line_cntr, 22, tax_group_line.amount )
                                            sheet.write(line_cntr, 23, (tax_group_line.amount*inv_line_id.price_subtotal)/100 )
                        sheet.write_merge(line_cntr, line_cntr, 26, 27, inv_id.partner_id.state_id.name )
                        line_cntr+=1
            stream = cStringIO.StringIO()
            print "streamstreamstream",stream
            workbook.save(stream)
            cr.execute(""" DELETE FROM output""")
            attach_id = self.env['output'].create({'name':Hedaer_Text+'.xls', 'xls_output': base64.encodestring(stream.getvalue())})
            print "attach_idattach_idattach_idattach_id",attach_id
            return {
                 'type' : 'ir.actions.act_url',
                 'url': '/opt/download?model=output&field=xls_output&id=%s&filename=GSTR-1.xls'%(attach_id.id),
                 'target': 'self',
                }        
GstReturnOne()