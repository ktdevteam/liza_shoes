from odoo import models, fields, api, _
from odoo.osv import expression, osv
from datetime import datetime,date
import re
from base64 import b64decode
import logging
import datetime
LOG = logging.getLogger("dicttoxml")
import binascii
import os
import string
import time
import xlwt
import cStringIO
import base64
#from .. import format_common
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition

class Binary(http.Controller):
 @http.route('/opt/download', type='http', auth="public")
 @serialize_exception
 def download_document(self,model,field,id,filename=None, **kw):
    """ Download link for files stored as binary fields.
    :param str model: name of the model to fetch the binary from
    :param str field: binary field
    :param str id: id of the record from which to fetch the binary
    :param str filename: field holding the file's name, if any
    :returns: :class:`werkzeug.wrappers.Response`
    """
    cr, uid, context = request.cr, request.uid, request.context
    env = api.Environment(cr, 1, {})  
    out_brw=env['output'].browse(int(id))
    filecontent = base64.b64decode(out_brw.xls_output.encode('utf-8') or '')
    if not filecontent:
        return request.not_found()
    else:
       if not filename:
           filename = '%s_%s' % (model.replace('.', '_'), id)
       return request.make_response(filecontent,
                      [('Content-Type', 'application/octet-stream'),
                       ('Content-Disposition', content_disposition(filename))])  
                       
class GstPurchaseReturn(models.TransientModel):
    _name = "gst.purchase.return.report"
    _description = 'GST Purchase Return Report'
    
    start_date=fields.Date('From')
    end_date=fields.Date('To')
    @api.multi
    def gst_purchase_return(self):
        cr= self.env.cr
        start_date=self.start_date
        print'start_date---',type(start_date)
        end_date=self.end_date
        inv_obj = self.env['account.invoice']
        company_obj = self.env['res.company']
#        self.env.cr.execute("select id from account_invoice where date_invoice::date between %s and %s",(start_date,end_date))
#        inv_ids = map(lambda x:x[0], self.env.cr.fetchall())
#        print'inv_ids===========',inv_ids
        inv_ids=inv_obj.search([('date_invoice','>=',start_date),('date_invoice','<=',end_date),('state','in',('open','paid')),('type','in',('in_invoice','in_refund'))])
        print'---------------------',inv_ids
        self.env.cr.execute("select id from res_company")
        company_id = map(lambda x:x[0], self.env.cr.fetchall())
        res_company=company_obj.search([('id','in',company_id)])
        print'res_company=========',company_id
        if inv_ids:
            workbook = xlwt.Workbook()
            sheet = workbook.add_sheet('Purchase Return')
            style1 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 300; borders: left thin, top thin, bottom thin,right thin')
            style2 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 200; borders: left thin, top thin, bottom thin,right thin')
            style3 = xlwt.easyxf('font: bold on; borders: left thin, top thin, bottom thin,right thin')
            Hedaer_Text = 'GSTR-1'
            sheet.write_merge(0, 1, 0, 7, 'Form GSTR-2', style1)
            sheet.write_merge(2, 3, 0, 7, '[See Rule.....]', style2)
            sheet.write_merge(5, 6, 0, 7, 'DETAILS OF INWARD SUPPLIES', style1)
            sheet.write_merge(8, 8, 0, 5, )
            date=datetime.datetime.strptime(start_date, "%Y-%m-%d").strftime('%Y')
            sheet.write(8, 6, 'Year', style3 )
            sheet.write(8, 7, date ,style2)
            sheet.write_merge(9, 9, 0, 5, )
            date=datetime.datetime.strptime(start_date, "%Y-%m-%d").strftime('%B')
            sheet.write(9, 6, 'Month', style3 )
            sheet.write(9, 7, date ,style2)
            sheet.write_merge(10, 10, 1, 5, 'GSTIN', style2)
            sheet.write_merge(10, 10, 6, 7, res_company.vat, style2)
            sheet.write_merge(11, 11, 1, 5, 'Name of the Taxable Person', style2)
            sheet.write_merge(11, 11, 6, 9, res_company.name, style2)
            sheet.write_merge(12, 12, 1, 5, 'Aggregate Turnover of the Taxable Person in the previous FY', style2)
            sheet.write_merge(13, 13, 1, 5, 'Period', style2 )
            sheet.write_merge(15, 15, 0, 1, 'GSTIN/UIN', style2 )
            sheet.write_merge(15, 15, 2, 3, 'PAN NO', style2 )
            sheet.write_merge(15, 15, 4, 4, 'Type', style2 )
            sheet.write_merge(15, 15, 5, 5, 'Invoice No', style2 )
            sheet.write_merge(15, 15, 6, 6, 'Invoice Date', style2 )
            sheet.write_merge(15, 15, 7, 8, 'Without tax', style2 )
            sheet.write_merge(15, 15, 9, 10, 'Taxable value', style2 )
            sheet.write_merge(15, 15, 11, 11, 'CGST', style2 )
            sheet.write_merge(15, 15, 12, 12, 'SGST', style2 )
            sheet.write_merge(15, 15, 13, 13, 'IGST', style2 )
            sheet.write_merge(15, 15, 14, 15, 'Place Of Supply', style2 )
            line_cntr=16
            for inv_id in inv_ids:
                if inv_id.type=='in_refund' or inv_id.type=='in_invoice':
                    sheet.write_merge(line_cntr, line_cntr, 0, 1, inv_id.partner_id.gst, style2 )
                    sheet.write_merge(line_cntr, line_cntr, 2, 3, inv_id.partner_id.pan_no, style2 )
                    if inv_id.type=='in_refund':
                        sheet.write_merge(line_cntr, line_cntr, 4, 4, 'Refund Invoice' ,style2 )
                    if inv_id.type=='in_invoice':
                       sheet.write_merge(line_cntr, line_cntr, 4, 4, "Invoice",style2  )
                    sheet.write_merge(line_cntr, line_cntr, 5, 5, inv_id.reference ,style2 )
                    sheet.write(line_cntr, 6, inv_id.date_invoice ,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 7, 8, inv_id.amount_untaxed ,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 9, 10, inv_id.amount_total ,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 11, 11, inv_id.sgst_total ,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 12, 12, inv_id.cgst_total ,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 13, 13, inv_id.igst_total,style2 )
                    sheet.write_merge(line_cntr, line_cntr, 14, 15, inv_id.partner_id.state_id.name ,style2 )
                    line_cntr+=1
            stream = cStringIO.StringIO()
            print "streamstreamstream",stream
            workbook.save(stream)
            cr.execute(""" DELETE FROM output""")
            attach_id = self.env['output'].create({'name':Hedaer_Text+'.xls', 'xls_output': base64.encodestring(stream.getvalue())})
            print "attach_idattach_idattach_idattach_id",attach_id
            return {
                 'type' : 'ir.actions.act_url',
                 'url': '/opt/download?model=output&field=xls_output&id=%s&filename=Purchase Return.xls'%(attach_id.id),
                 'target': 'self',
                }        
GstPurchaseReturn()