from odoo import models, fields, api, _
from odoo.osv import expression, osv
from datetime import datetime,date
import re
from base64 import b64decode
import logging
import datetime
LOG = logging.getLogger("dicttoxml")
import binascii
import os
import string
import time
import xlwt
import cStringIO
import base64
#from .. import format_common
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition

class Binary(http.Controller):
 @http.route('/opt/download', type='http', auth="public")
 @serialize_exception
 def download_document(self,model,field,id,filename=None, **kw):
    """ Download link for files stored as binary fields.
    :param str model: name of the model to fetch the binary from
    :param str field: binary field
    :param str id: id of the record from which to fetch the binary
    :param str filename: field holding the file's name, if any
    :returns: :class:`werkzeug.wrappers.Response`
    """
    cr, uid, context = request.cr, request.uid, request.context
    env = api.Environment(cr, 1, {})  
    out_brw=env['output'].browse(int(id))
    filecontent = base64.b64decode(out_brw.xls_output.encode('utf-8') or '')
    if not filecontent:
        return request.not_found()
    else:
       if not filename:
           filename = '%s_%s' % (model.replace('.', '_'), id)
       return request.make_response(filecontent,
                      [('Content-Type', 'application/octet-stream'),
                       ('Content-Disposition', content_disposition(filename))])  
                       
class OutwordReturn(models.TransientModel):
    _name = "outword.return.report"
    _description = 'Outword Return'
    
    start_date=fields.Date('From')
    end_date=fields.Date('To')
    @api.multi
    def gst_outword_return(self):
        cr= self.env.cr
        start_date=self.start_date
        print'start_date---',type(start_date)
        end_date=self.end_date
        inv_obj = self.env['account.invoice']
        company_obj = self.env['res.company']
        inv_ids=inv_obj.search([('date_invoice','>=',start_date),('date_invoice','<=',end_date),('state','in',('open','paid')),('type','in',('out_invoice','out_refund'))])
        print'---------------------',inv_ids
        tax_obj = self.env['account.tax']
        if inv_ids:
            b2b_inv_id=[]
            b2cl_inv_id=[]
            b2cs_inv_id=[]
            workbook = xlwt.Workbook()
            zero_row_zero='GSTTIN/UIN of Recipient'
            zero_row_first='Invoice Number'
            zero_row_second='Invoice Date'
            zero_row_third='Invoice Value'
            zero_row_fourth='Place Of Supply'
            zero_row_fifth='Reverse Charge'
            zero_row_sixth='Invoice Type'
            zero_row_seventh='E-commerce GSTIN.'
            zero_row_eighth='Rate'
            zero_row_nineth='Taxable value'
            zero_row_tenth='Cess Amount'
            style2 = xlwt.easyxf('alignment: horiz centre;')
            style3 = xlwt.easyxf('alignment: horiz centre;font: bold on; borders: left thin, top thin, bottom thin,right thin')
            Hedaer_Text = 'Outword Return'
            for inv_id in inv_ids:
                if inv_id.partner_id.gst:
                    b2b_inv_id.append(inv_id)
                    print'b2b_inv_id------',b2b_inv_id
                if not inv_id.partner_id.gst and inv_id.amount_total>250000:
                    b2cl_inv_id.append(inv_id)
                    print'b2cl-=========',b2cl
                if not inv_id.partner_id.gst and inv_id.amount_total < 250000:
                    b2cs_inv_id.append(inv_id)
                    print'b2cs-=========',b2cs_inv_id
            if b2b_inv_id:
                sheet = workbook.add_sheet('b2b')
                sheet.write(0, 0, zero_row_zero, style3)
                sheet.write(0, 1, zero_row_first, style3)
                sheet.write(0, 2, zero_row_second, style3)
                sheet.write(0, 3, zero_row_third, style3)
                sheet.write(0, 4, zero_row_fourth, style3)
                sheet.write(0, 5, zero_row_fifth, style3)
                sheet.write(0, 6, zero_row_sixth, style3)
                sheet.write(0, 7, zero_row_seventh, style3)
                sheet.write(0, 8, zero_row_eighth, style3)
                sheet.write(0, 9, zero_row_nineth, style3)
                sheet.write(0, 10, zero_row_tenth, style3)
                sheet.col(0).width = 200 * 30
                sheet.col(1).width = 150 * 30
                sheet.col(2).width = 200 * 30
                sheet.col(3).width = 150 * 40
                sheet.col(4).width = 150 * 30
                sheet.col(5).width = 150 * 30
                sheet.col(6).width = 150 * 30
                sheet.col(7).width = 160 * 30
                sheet.col(8).width = 150 * 30
                sheet.col(9).width = 150 * 30
                sheet.col(10).width = 150 * 30
                line_cntr=1
            for inv_id in b2b_inv_id:
                for inv_tax in inv_id.tax_line_ids:
                    if inv_id.partner_id.gst:
                        sheet.write(line_cntr, 0, inv_id.partner_id.gst, style2)
                        sheet.write(line_cntr, 1, inv_id.number, style2)
                        sheet.write(line_cntr, 2, inv_id.date_invoice, style2)
                        sheet.write(line_cntr, 3, inv_id.amount_total, style2)
                        sheet.write(line_cntr, 4, inv_id.partner_id.state_id.code+'-'+inv_id.partner_id.state_id.name, style2)
                        sheet.write(line_cntr, 5, 'No', style2)
                        sheet.write(line_cntr, 6, 'Regular', style2)
                        tax_id=tax_obj.search([('name','=',inv_tax.name)])
                        print'tax_id-----------',tax_id
                        if tax_id and len(tax_id)==1:
                            sheet.write(line_cntr, 8, tax_id.amount, style2)
                        sheet.write(line_cntr, 9, inv_tax.amount, style2)
                    line_cntr+=1
            if b2cl_inv_id:
                sheet = workbook.add_sheet('b2cl')
                sheet.write(0, 0, zero_row_first, style3)
                sheet.write(0, 1, zero_row_second, style3)
                sheet.write(0, 2, zero_row_third, style3)
                sheet.write(0, 3, zero_row_fourth, style3)
                sheet.write(0, 4, zero_row_fifth, style3)
                sheet.write(0, 5, zero_row_eighth, style3)
                sheet.write(0, 6, zero_row_nineth, style3)
                sheet.write(0, 7, zero_row_tenth, style3)
                sheet.col(0).width = 200 * 30
                sheet.col(1).width = 150 * 30
                sheet.col(2).width = 200 * 30
                sheet.col(3).width = 150 * 40
                sheet.col(4).width = 150 * 30
                sheet.col(5).width = 150 * 30
                sheet.col(6).width = 150 * 30
                sheet.col(7).width = 160 * 30
                line_cntr=1
            for inv_id in b2cl_inv_id:
                for inv_tax in inv_id.tax_line_ids:
                    sheet.write(line_cntr, 0, inv_id.number, style2)
                    sheet.write(line_cntr, 1, inv_id.date_invoice, style2)
                    sheet.write(line_cntr, 2, inv_id.amount_total, style2)
                    sheet.write(line_cntr, 3, inv_id.partner_id.state_id.code+'-'+inv_id.partner_id.state_id.name, style2)
                    tax_id=tax_obj.search([('name','=',inv_tax.name)])
                    print'tax_id-----------',tax_id
                    if tax_id and len(tax_id)==1:
                        sheet.write(line_cntr, 4, tax_id.amount, style2)
                    sheet.write(line_cntr, 5, inv_tax.amount, style2)
                    line_cntr+=1
            if b2cs_inv_id:
                sheet = workbook.add_sheet('b2cs')
                sheet.write(0, 0, zero_row_sixth, style3)
                sheet.write(0, 1, zero_row_fourth, style3)
                sheet.write(0, 2, zero_row_eighth, style3)
                sheet.write(0, 3, zero_row_nineth, style3)
                sheet.write(0, 4, zero_row_tenth, style3)
                sheet.write(0, 5, zero_row_seventh, style3)
                sheet.col(0).width = 200 * 30
                sheet.col(1).width = 150 * 30
                sheet.col(2).width = 200 * 30
                sheet.col(3).width = 150 * 40
                sheet.col(4).width = 150 * 30
                line_cntr=1
            for inv_id in b2cs_inv_id:
                for inv_tax in inv_id.tax_line_ids:
                    sheet.write(line_cntr, 0, inv_id.number, style2)
                    sheet.write(line_cntr, 1, inv_id.date_invoice, style2)
                    tax_id=tax_obj.search([('name','=',inv_tax.name)])
                    print'tax_id-----------',tax_id
                    if tax_id and len(tax_id)==1:
                        sheet.write(line_cntr, 2, tax_id.amount, style2)
                    sheet.write(line_cntr, 3, inv_tax.amount, style2)
                    line_cntr+=1
            stream = cStringIO.StringIO()
            print "streamstreamstream",stream
            workbook.save(stream)
            cr.execute(""" DELETE FROM output""")
            attach_id = self.env['output'].create({'name':Hedaer_Text+'.xls', 'xls_output': base64.encodestring(stream.getvalue())})
            print "attach_idattach_idattach_idattach_id",attach_id
            return {
                 'type' : 'ir.actions.act_url',
                 'url': '/opt/download?model=output&field=xls_output&id=%s&filename=Outword Return.xls'%(attach_id.id),
                 'target': 'self',
                }        
OutwordReturn()