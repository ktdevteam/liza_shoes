from openerp import api, fields, models
import datetime

class PosOrder(models.Model):

    _inherit = "pos.order"

    refund_type = fields.Selection([('no','No'),('partial','Partial'),('full','Full')],string='Refuned',default='no', readonly=1)
    get_subtotal_in_words=fields.Char()

    @api.model
    def create(self, vals):
        parent_id=False
        if vals.has_key('parent_id'):
            
            orders = self.search([('id', '=', vals['parent_id'])])
            parent_id=orders.id
            print"vals.get('is_refunded',False)vals.get('is_refunded',False)",orders
            total_qty=sum(line.qty for line in orders.lines)
            print"total_qtytotal_qtytotal_qty",total_qty
            
            return_qty=sum(line[2]['qty'] for line in vals.get('lines'))
            print"return_qtyreturn_qtyreturn_qtyreturn_qty",return_qty
#            is_refunded='no'
            if return_qty<total_qty:
                is_refunded='partial'
            elif return_qty==total_qty:
                is_refunded='full'
#            orders.write({
##                'is_refunded': is_refunded,
#                'refund_date': datetime.datetime.now(),
#            })
            if vals.has_key('old_lines'):
                old_lines=vals.get('old_lines')
                for line in orders.lines.filtered(lambda l:l.id in [int(l) for l in old_lines]):
                    line.write({'refund_qty':line.refund_qty+float(old_lines[str(line.id)])})
            vals.pop('parent_id');
            vals.pop('old_lines');
#            vals.pop('is_refunded');
        new_order=super(PosOrder, self).create(vals)
        new_order.parent_id=parent_id
        if  new_order.parent_id:
            new_order.parent_id.write({'refund_type': is_refunded,})
        return new_order

    @api.model
    def _order_fields(self, ui_order): #v10
        vals = super(PosOrder, self)._order_fields(ui_order)
        if ui_order.has_key('is_refund'):
            vals['is_refund'] = True
            vals['source'] = ui_order['source']
            vals['parent_id'] = ui_order['parent_id']
            vals['old_lines'] = ui_order['old_lines']
#            vals['is_refunded'] = ui_order['is_refunded']
        return vals

class PosOrderLine(models.Model):

    _inherit = "pos.order.line"

    refund_qty = fields.Float('Refund/Return Qty')
