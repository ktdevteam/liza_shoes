odoo.define('pos_refund_order.refund_extend', function(require) {
"use strict"
var core = require('web.core');
var Model = require('web.DataModel');
var screens = require('point_of_sale.screens');
var ScreenWidget = screens.ScreenWidget;
var pos_refund_order= require('pos_refund_order');
var gui = require('point_of_sale.gui');
var QWeb = core.qweb;
var db = require('point_of_sale.DB');

var pos_model = require('point_of_sale.models');
var _modelproto = pos_model.PosModel.prototype;
var models = _modelproto.models;

 db.include({
     add_orders: function(orders) {
            var updated_count = 0;
            if(!orders instanceof Array){
                orders = [orders];
            }
            this.order_by_id={};
            this.orders_sorted=[];
            for(var i = 0, len = orders.length; i < len; i++){
                var order = orders[i];
                console.log("orderorderorderorderorder",order,order.refund_type);
                 if (order.refund_type!=='full'){
                    this.order_by_code_reference[order.pos_reference] = order;
                    this.orders_sorted.push(order);
                    this.order_by_id[order.id] = order;
                    this.order_search_string += this._order_search_string(order);
                    updated_count += 1;
                    }
            }
            return updated_count;
        },
        add_order_lines: function(lines) {
            if (!lines instanceof Array) {
                lines = [lines];
            }
            var count_up = 0;
            this.line_by_id=[];
            this.line_by_pos_reference={};
            for(var i = 0, len = lines.length; i < len; i++){
                var line = lines[i];
                this.line_by_id[line.id] = line;
                count_up += 1
                if (!this.line_by_pos_reference[line.pos_reference]) {
                    this.line_by_pos_reference[line.pos_reference] = []
                    this.line_by_pos_reference[line.pos_reference].push(line);
                } else {
                    this.line_by_pos_reference[line.pos_reference].push(line);
                }
            }
            console.log("this.line_by_pos_reference",this.line_by_pos_reference);
            console.log('add new line: ' + count_up)
        },
 })
 
for (var i = 0; i < models.length; i++) {
    var model = models[i];
    if (model.model === 'pos.order') {
        model.domain =  function(self){
            return [['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refund', '=', false], ['refund_type', '!=','full']];
        };
    };
}

//load new field 'refund_qty'
pos_model.load_fields("pos.order.line", "refund_qty");
pos_model.load_fields("pos.order", ["refund_type"]);


pos_refund_order.RefundScreenWidget.include({
    init: function(parent, options){
        this._super(parent, options);
        this.refund_order_cache = new screens.DomCache();
    },
    show: function(){
            var self = this;
            this._super();
            var e2key = function(e) {
                console.log(e.which)
                if (!e) return '';
                if (e.which == 82 && self.refund) {
                    self.made_refund_order();
                };
                if (e.which == 81) {
                    self.gui.show_screen('products');
                }
                if (e.which == 83) {
                    $('.searchbox >input').focus()
                }
            };

            var page5Key = function(e, customKey) {
                if (e) e.preventDefault();
                switch(e2key(customKey || e)) {
                    case 'left': /*...*/ break;
                    case 'right': /*...*/ break;
                }
            };

            $(document).bind('keyup', page5Key);
            $(document).trigger('keyup', [{preventDefault:function(){},keyCode:37}]); 

            var orders = this.pos.db.get_orders_sorted();
            this.render_list(orders);
            this.reload_orders();
            var search_timeout = null;
            this.$('.order-list-contents').delegate('.client-line','click',function(event){
                var element =  $(this);
                var code = element.data('code')
                self.line_select(event, element, code);
            });
            this.$('.searchbox input')[0].value = '';
            this.$('.back').click(function(){
                self.gui.show_screen('products');
            });
            this.orders = []
            this.el.querySelector('.searchbox input').addEventListener('keyup',function (event) {
                clearTimeout(search_timeout);
                var query = this.value;
                search_timeout = setTimeout(function(){
                    self.perform_search(query,event.which === 13);
                },70);
            });
            this.pos.barcode_reader.set_action_callback({
                'order': self.barcode_order_action ? function(code){ self.barcode_order_action(code); } : undefined ,
            });
            var order = this.order;
        },
    reload_orders: function(){
        var self = this;
//         return this.pos.load_new_orders().then(function(){
//             self.render_list(self.pos.db.get_orders_sorted())
//         });
        return this.pos.load_new_orders().then(function(){
            var orders = self.pos.db.get_orders_sorted()
            var pos_references = [];
            for (var i = 0, len = orders.length; i < len; i++) {
                    pos_references.push(orders[i].pos_reference);
            }
            console.log("pos_referencespos_references",pos_references);
            var domain=[['pos_reference', 'in', pos_references]];
            return self.pos.load_new_order_lines(domain).then(function(){;
            self.render_list(self.pos.db.get_orders_sorted())});
        })
        ;
    },
    made_refund_order: function() {
        var self=this;
        self.gui.show_popup('refund-line-info-input');
    },
    line_select: function(event, element, code){
        var order = this.pos.db.get_order_by_code_reference(code);
        var contents = this.$('.order-details-contents');
        contents.empty();
        this.order_selected = order;
        this.pos.order_selected = order;
        this.lines = this.pos.db.get_line_by_pos_reference(order.pos_reference);
        var all_element_actived = this.$('.highlight');
        var highlight =  element.hasClass('highlight');
        if (all_element_actived.length == 0){
            element.addClass('highlight');

        } else {
            this.$('.order-list-contents .highlight').removeClass('highlight');
            element.addClass('highlight');

        }
            contents.append($(QWeb.render('OrderInformationWidget',{ widget: this, order: order})));
        this.show_refund_button();
    },
    render_list: function(orders){
        var contents = this.$el[0].querySelector('.order-list-contents');
        contents.innerHTML = "";
        for(var i = 0, len = Math.min(orders.length,1000); i < len; i++){
            var order    = orders[i];
            var orderline = this.refund_order_cache.get_node(order.id);
            var orderline_html = QWeb.render('RecoverOrders',{widget: this, order: order});
            var orderline = document.createElement('tbody');
            orderline.innerHTML = orderline_html;
            orderline = orderline.childNodes[1];
            contents.appendChild(orderline);
            this.refund_order_cache.cache_node(order.id,orderline);
        }
    },
    close: function(){
        this._super();
    },
});
var _super_order = pos_model.Order.prototype;
pos_model.Order = pos_model.Order.extend({
    export_as_JSON: function () {
        return _.extend(_super_order.export_as_JSON.apply(this, arguments), {
            is_refunded:this.refund_type,
            old_lines:this.old_lines,
        });
    },
    

});
var PosModelSuper=pos_model.PosModel;
pos_model.PosModel = pos_model.PosModel.extend({
    
    _flush_orders: function (orders, options) {
        var result_new = PosModelSuper.prototype._flush_orders.call(this, orders, options);
        var self = this;
        var new_order = {};
        var return_order_list = self.orders;
        console.log('ffffffffffffffffff');
        self.load_new_orders();
//        for (var i in orders) {
//            console.log("ordersordersordersorders->>>>>>>>>>>>>>>",orders);
//            var partners = self.partners;
//            var partner = "";
//            for(var j in partners){
//                if(partners[j].id == orders[i].data.partner_id){
//                    partner = partners[j].name;
//                }
//            }
//            new_order = {
//                'amount_tax': orders[i].data.amount_tax,
//                'amount_total': orders[i].data.amount_total,
//                'pos_reference': orders[i].data.name,
//                'partner_id': [orders[i].data.partner_id, partner],
//                'session_id': [
//                    self.pos_session.id, self.pos_session.name
//                ]
//            };
//            return_order_list.push(new_order);
//            self.orders = return_order_list;
//            self.gui.screen_instances.refund_screen.render_list(return_order_list);
//        }
        return result_new;
    },
//pos_model.PosModel.include({
load_new_orders: function(){
        var self = this;
        var def  = new $.Deferred();
        var tmp = {}; 
        var fields = _.find(this.models,function(model){ return model.model === 'pos.order'; }).fields;
        new Model('pos.order')
            .query(fields)
            .filter([['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refund', '=', false],['refund_type', '!=','full']])
//            .filter([['state', '!=', 'draft'], ['state', '!=', 'cancel'], ['is_refund', '=', false], ['refund_type', '!=','full']])
            .all({'timeout':3000, 'shadow': true})
            .then(function(orders){
                console.log('ordersordersordersorders99999',orders);
                if (self.db.add_orders(orders)) {   // check if the partners we got were real updates
                    def.resolve();
                } else {
                    def.reject();
                }
            }, function(err,event){ event.preventDefault(); def.reject(); });    
        return def;
    },


load_new_order_lines: function(domain){
        var self = this;
        var def  = new $.Deferred();
        var fields = _.find(this.models,function(model){ return model.model === 'pos.order.line'; }).fields;
//        var domain = _.find(this.models,function(model){ return model.model === 'pos.order.line'; }).domain;
        new Model('pos.order.line')
            .query(fields)
            .filter(domain)
            .all({'timeout':3000, 'shadow': true})
            .then(function(lines){
                if (self.db.add_order_lines(lines)) {   // check if the partners we got were real updates
                    def.resolve();
                } else {
                    def.reject();
                }
            }, function(err,event){ event.preventDefault(); def.reject(); });    
        return def;
    },
});

})