{
    'name': 'POS Refund the orders1',
    'sequence': 0,
    'version': '1.4',
    'author': 'knacktechs solutions',
    'category': 'Point of Sale',
    'depends': ['pos_base','pos_refund_order'],
    'data': [
        'template/template.xml',
        'view/pos.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'application': True,
    'price': '0',
    "currency": 'EUR',
    'images': ['static/description/icon.png'],
    'license': 'LGPL-3',
}
